# painlessMeshListener

This is a simple listen server that can be used in conjunction with a [painlessMesh](https://gitlab.com/BlackEdder/painlessMesh) mesh. For more details on how to set up painlessMesh to log to this listener see the [wiki](https://gitlab.com/BlackEdder/painlessMesh/wikis/bridge-between-mesh-and-another-network)


## Install

Easiest way to get the listener to run is to clone the git repository and compile it using `dub`. Dub will download the needed dependencies, compile the code and run it. 

```
git clone https://BlackEdder@gitlab.com/BlackEdder/painlessMeshListener.git
cd painlessMeshListener
dub
```

Alternatively, after compilation by dub you can move the resulting binary (`painlessmeshlistener`) to a location of your choosing.

`dub` is part of the D compiler. See below for installation instructions 

## D Compiler installation

### Desktop/server

On most computers the easiest way to install D and dub is to install DMD. For instructions see [here](https://dlang.org)

### ARM based machine (raspberry-pi)

DMD does not have support for the ARM architecture yet (raspberry-pi). So on an ARM machine you will need to install a version of [LDC](https://github.com/ldc-developers/ldc/releases). Currently the latest LDC version pre build for the arm is version 1.1.0. You can install this version as follows:

```
wget https://github.com/ldc-developers/ldc/releases/download/v1.1.0/ldc2-1.1.0-linux-armhf.tar.xz
tar xf ldc2-1.1.0-linux-armhf.tar.xz
sudo mv ldc2-1.1.0-linux-armhf /opt/
sudo apt-get install libevent-dev libssl-dev
sudo apt-get install gcc
```

Add the bin subdirectory to your path, following method will add path temporally, for permanent PATH you need to edit ~/.profile file.

```
export PATH="/opt/ldc2-1.1.0-linux-armhf/bin:$PATH"
```

Now dub should be properly installed and you can follow the regular installation instructions.


## Changing the code

The listener is currently setup to log all the received messages to the console and to notify the rest of the mesh that they can log to this listener. If you want to change this behaviour then the best places to start are at 

- [log](https://gitlab.com/BlackEdder/painlessMeshListener/blob/master/source/app.d#L36)
- [broadcast](https://gitlab.com/BlackEdder/painlessMeshListener/blob/master/source/app.d#L42)
